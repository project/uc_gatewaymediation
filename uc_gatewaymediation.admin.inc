<?php
/**
 * @file
 * The admin functions for the uc_gatewaymediation module.
 */

 /**
 * Implementation of hook_form_alter().
 */
function uc_gatewaymediation_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'uc_gatewaymediation_form':
    break;
  }
}
function _gatewaymediation_list_all_gateways($filter = NULL, $enabled_only = FALSE) {
  $gateways = module_invoke_all('payment_gateway');

  foreach ($gateways as $i => $value) {
    if ($value['id']!= 'gatewaymediation') {
      $gateways[$i]['enabled'] = variable_get('uc_pg_'. $gateways[$i]['id'] .'_enabled', TRUE);
      if ($filter != NULL) {
        if (!isset($gateways[$i][$filter]) || !function_exists($gateways[$i][$filter])) {
          unset($gateways[$i]);
        }
      }
      if ($enabled_only) {
        if (!variable_get('uc_pg_'. $gateways[$i]['id'] .'_enabled', TRUE)) {
          unset($gateways[$i]);
        }
      }
    } 
    else {
      unset($gateways[$i]);
    }
  }

  return $gateways;
}
function uc_gatewaymediation_admin_settings() {
  $gateways = _gatewaymediation_list_all_gateways('credit'); // all gateways defined in ubercart
  $form = array();

  $form['uc_gatewaymediation'] = array(
    '#summary callback' => '_uc_gatewaymediation_summarize',
    '#summary arguments' => array($gateways),
  );

  if (is_array($gateways) && count($gateways) > 0) {
    $form['uc_gatewaymediation_info'] = array(
      '#value' => '<div><strong>'. t('Gateway mediation') .'</strong><br />'
                . t('Gateways mediation allows you to process payments using more than one gateway.') . '</div><br />'
                . '<div><strong>' . t('Select the gateway to use for each card that is to be mediated') . '</strong></div>',
      '#weight' => -10,
    );
    
    $cards = _list_cards();
    $options = array();
    $options[0] = 'Select...';
    foreach ($gateways as $gateway) {
      $options[$gateway['id']] = $gateway['id'];
    }
    $weight = -9;
    foreach ($cards as $card) {
      $form['uc_gatewaymediation_info']['cards']['uc_gatewaymediation_info_' . $card['field']] = array(
        '#type' => 'select',
        '#weight' => $weight++,
        '#title' => $card['title'],
        '#multiple' => FALSE,
        '#size' => 0,
        '#options' => $options,
        '#default_value' => variable_get('uc_gatewaymediation_info_' . $card['field'], 0),
      );
    }
  return system_settings_form($form);
  }
}
// Returns an array of enabled payment gateways for the form summary.
function _uc_gatewaymediation_summarize($form, $gateways) {

  $items = array();
  $cards = _list_cards();
  if (is_array($cards)) {
    foreach ($cards as $card) {
       if ($gateway = variable_get('uc_gatewaymediation_info_' . $card['field'], '0')) {
         $items[] = t('!card transactions will be assigned to the !gateway gateway', array('!card' => $card['title'], '!gateway' => $gateway));
       }
    }
  } 
  else {
    $items[] = $cards;
  }
  return $items;
}

// Return an array of credit cards
function _list_cards($titleonly = FALSE) {
  $ccs = variable_get('uc_credit_accepted_types', NULL);
  if (empty($ccs)) {
    $items = t('No credit card types are currently enabled');
  } 
  else {
    $items = array();
    $cards = explode(chr(13) . chr(10), $ccs);
    foreach ($cards as $card) {
      if (!$titleonly) {
        $cardarray = array();
        $cardarray['title'] = $card;
        $cardarray['field'] = str_replace(' ', '_', $card);
      } 
      else {
        $cardarray = $card;        
      }
      $items[] = $cardarray;
    }
  }
  return $items;
}
